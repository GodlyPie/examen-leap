//La funcion recibe un entero "n"
boolean xyz (int n)
{
	float i = math.sqrt(n); //Se saca la raiz cuadrada de "n" y se guarda ese valor en "i".
	int j = math.ceil(i); //Se redondea "i" al entero superior mas cercano y se guarda en "j".
	int k = 2; //Se le asigna a "k" el valor de 2.
	int x = k; //A "x" se le asigna el valor de "k", osea 2.
	while(x <= j) //Se crea un loop que durará mientras que "x" sea menor o igual a "j".
	{
		if(!(n % x)) return false; //Si "n" modulo "x" es cero la funcion retorna falso
		else x++; // De lo contrario a "x" se le suma 1
	}
	return true; //Si sale del loop, cuando x se vuelve mayor que j, entonces retorna verdadero
}