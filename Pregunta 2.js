function colision3Rectangulos(r1, r2, r3)
{
	var output = 0;
	if(!((r1.x + r1.width) < r2.x || (r1.y + r1.height) < r2.y || r1.x > (r2.x + r2.width) || r1.y > (r2.y + r2.height)))
		output += 1;
	if(!((r1.x + r1.width) < r3.x || (r1.y + r1.height) < r3.y || r1.x > (r3.x + r3.width) || r1.y > (r3.y + r3.height)))
		output += 2;
	if(!((r2.x + r2.width) < r3.x || (r2.y + r2.height) < r3.y || r2.x > (r3.x + r3.width) || r2.y > (r3.y + r4.height)))
		output += 4;

	return output;

	/*
	output igual a 
	000 -> 0: no hay colision.
	001 -> 1: colision entre r1 - r2.
	010 -> 2: colision entre r1 - r3.
	011 -> 3: colision entre r1 - r2 y r1 - r3.
	100 -> 4: colision entre r2 - r3.
	101 -> 5: colision entre r1 - r2 y r2 - r3.
	110 -> 6: colision entre r1 - r3 y r2 - r3.
	111 -> 7: colision entre r1 - r2, r1 - r3 y r2 - r3.  
	*/
}