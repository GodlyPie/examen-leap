function Enemy(game, x, y, direction, objective, map)
{
  	Phaser.Sprite.call(this, game, x, y, "enemy");
  	this.speed = 350;
  	this.objective = parseInt(objective);
  	this.direction = parseInt(direction);

  	this.map = map;
  	this.start = this.map.getTileWorldXY(this.x, this.y);

  	this.achieved = false;

    game.physics.arcade.enable(this);
    this.body.collideWorldBounds = true;
}

Enemy.prototype = Object.create(Phaser.Sprite.prototype);

Enemy.prototype.constructor = Enemy;
Enemy.prototype.update = function()
{
	this.actual = this.map.getTileWorldXY(this.x, this.y);
	if(!this.achieved)
	{
		//console.log(this.direction);
		if(this.direction == 1)
		{
			this.body.velocity.x = +this.speed;
			if(this.actual.x > (this.start.x + this.objective))
				this.achieved = true;
		}
		if(this.direction == 2)
		{
			this.body.velocity.x = -this.speed;
			if(this.actual.x < (this.start.x - this.objective))
				this.achieved = true;
		}
		if(this.direction == 3)
		{
			this.body.velocity.y = +this.speed;
			if(this.actual.y > (this.start.y + this.objective))
				this.achieved = true;
		}	
		if(this.direction == 4)
		{
			this.body.velocity.y = -this.speed;
			if(this.actual.y < (this.start.y - this.objective))
				this.achieved = true;
		}
	}
	else
	{
		this.achieved = false;
  		this.start = this.map.getTileWorldXY(this.x, this.y);

  		if(this.direction == 1)
			this.direction = 2;
  		else if(this.direction == 2)
			this.direction = 1;
  		
  		if(this.direction == 3)
			this.direction = 4;
  		else if(this.direction == 4)
			this.direction = 3;
	}
};