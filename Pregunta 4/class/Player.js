function Player(game, x, y, map)
{
  	Phaser.Sprite.call(this, game, x, y, "player");
  	this.speed = 250;
  	this.map = map;
  	this.arrow = game.input.keyboard.createCursorKeys();
    game.physics.arcade.enable(this);
    this.body.collideWorldBounds = true;
}

Player.prototype = Object.create(Phaser.Sprite.prototype);

Player.prototype.constructor = Player;
Player.prototype.update = function()
{
	this.actual = this.map.getTileWorldXY(this.x, this.y);
	if(this.arrow.right.isDown)
	{
		this.body.velocity.x = +this.speed;
		//console.log("→");
	}
	if(this.arrow.left.isDown)
	{
		this.body.velocity.x = -this.speed;
		//console.log("←");
	}
	if(!this.arrow.right.isDown && !this.arrow.left.isDown || 
		this.arrow.right.isDown && this.arrow.left.isDown)
	{
		this.body.velocity.x = 0;
	}

	if(this.arrow.down.isDown)
	{
		this.body.velocity.y = +this.speed;
		//console.log("↓");
	}
	if(this.arrow.up.isDown)
	{
		this.body.velocity.y = -this.speed;
		//console.log("↑");
	}
	if(!this.arrow.down.isDown && !this.arrow.up.isDown || 
		this.arrow.down.isDown && this.arrow.up.isDown)
	{
		this.body.velocity.y = 0;
	}
};