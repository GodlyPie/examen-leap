
var GAME_WIDTH = 800;
var GAME_HEIGHT = 608;
var entities = {};
var variables = {};

var game = new Phaser.Game(GAME_WIDTH, GAME_HEIGHT, Phaser.AUTO, '', { preload: preload, create: create, update: update });

function preload()
{
	game.physics.startSystem(Phaser.Physics.ARCADE);
	game.load.image("player","assets/player.png?a=2");
	game.load.image("enemy","assets/enemy.png?a=2");
	game.load.tilemap("map", "assets/map.json?a=2", null, Phaser.Tilemap.TILED_JSON);
    game.load.image("tiles", "assets/tiles.png?a=2");
}

function create()
{
	variables.deaths = 0;
	entities.map = game.add.tilemap("map");
    entities.map.addTilesetImage("tiles", "tiles");

    entities.floorLayer = entities.map.createLayer("floor");
    entities.collisionLayer = entities.map.createLayer("collision");
	entities.map.setCollisionBetween(1, 475, true, entities.collisionLayer);

	entities.player = game.add.existing(new Player(game, entities.map.objects.objects[0].x,
	 	entities.map.objects.objects[0].y,
	 	entities.map));

	entities.enemies = game.add.group();
	for (var i = 1; i < entities.map.objects.objects.length-1; i++)
	{
		entities.enemies.add(new Enemy(game, entities.map.objects.objects[i].x,
			entities.map.objects.objects[i].y,
			entities.map.objects.objects[i].properties.direction,
			entities.map.objects.objects[i].properties.objective,
			entities.map));
	};

	entities.finish = entities.map.objects.objects[entities.map.objects.objects.length-1];

	entities.deathCounter = game.add.text(0,0,"Deaths: 0");
	entities.deathCounter.addColor('#aaaaaa', 0);

	entities.winBanner = game.add.text(GAME_WIDTH*.5,GAME_HEIGHT*.5,"You Win!");
	entities.winBanner.addColor('#aaaaaa', 0);
	entities.winBanner.anchor.set(.5);
	entities.winBanner.visible = false;
}

function update()
{
	game.physics.arcade.collide(entities.player, entities.collisionLayer);
	game.physics.arcade.overlap(entities.player, entities.enemies, null, function()
		{
			entities.player.reset(entities.map.objects.objects[0].x, entities.map.objects.objects[0].y);
			variables.deaths++;
		}, this);
	entities.deathCounter.text = "Deaths: "+variables.deaths;

	if(entities.player.actual == entities.map.getTileWorldXY(entities.finish.x, entities.finish.y))
	{
		entities.winBanner.visible = true;
	}
}