function dibujarSemiCircunferenciaNPoints(point, radius, n)
{
	var angle = 180/(n-1);

    for (var i = 0; i <= 180; i+=angle)
    {	
    	var radians = i*Math.PI/180;
    	var x = radius*Math.cos(radians);
    	var y = radius*Math.sin(radians);
        
    	drawCircle(point.x + x, point.y - y, 5);
    }
}